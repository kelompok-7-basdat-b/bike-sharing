from django import forms
from django.db import connection

STATUS_CHOICES=[
    ('tersedia', 'Tersedia'),
    ('tidak_tersedia', 'Tidak Tersedia'),
]

class CreateForm(forms.Form):
    error_messages = {
        'required': 'Please fill in the input',
    }

    merk = forms.CharField(label='Merk', max_length=10, required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    jenis = forms.CharField(label='Jenis', max_length=50, required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    status = forms.CharField(label='Status', widget=forms.Select(attrs={'class': 'form-control'}, choices=STATUS_CHOICES), required=True)
    stasiun = forms.CharField(label='Station', widget=forms.Select(attrs={'class': 'form-control'}, choices=STATUS_CHOICES), required=False)
    penyumbang = forms.CharField(label='Donator', widget=forms.Select(attrs={'class': 'form-control'}, choices=STATUS_CHOICES), required=False)

class UpdateForm(forms.Form):
    error_messages = {
        'required': 'Please fill in the input',
    }

    lat = forms.IntegerField(label='Latitude', required=False)
    lon = forms.IntegerField(label='Longitude', required=False)


