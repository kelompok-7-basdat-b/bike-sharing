from django.db import models

# Create your models here.

class Sepeda(models.Model):
    nomor = models.AutoField(primary_key=True)
    merk = models.CharField(max_length=10)
    jenis = models.CharField(max_length=50)
    status = models.BooleanField()
    id_stasiun = models.CharField(max_length=10)
    no_kartu_penyumbang = models.CharField(max_length=20)