from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .forms import CreateForm, UpdateForm
from .models import Sepeda

response = {}

def sepeda(request):
    message = Sepeda.objects.all()
    response['sepeda_list'] = message
    html = 'sepeda/sepeda.html'
    return render(request, html, response)

def create(request):
    html = 'sepeda/create.html'
    response['createform'] = CreateForm
    return render(request, html, response)

def create_sepeda(request):
    form = CreateForm(request.POST)
    if(request.method == 'POST' and form.is_valid()):
        prev_nomor = Sepeda.objects.raw("Select nomor from sepeda order by station_id DESC limit 1")
        nomor = int(prev_nomor[0].nomor) + 1
        merk = form.cleaned_data['merk']
        jenis = form.cleaned_data['jenis']
        status = form.cleaned_data['status']
        id_stasiun = form.cleaned_data['id_stasiun']
        no_kartu_penyumbang = form.cleaned_data['no_kartu_penyumbang']
        with connection.cursor() as cursor:
            cursor.execute("insert into sepeda values(%s, %s, %s, %s, %s, %s)", [nomor, merk, jenis, status, id_stasiun, no_kartu_penyumbang])
        return HttpResponseRedirect('/sepeda/table')
    else:
        return HttpResponseRedirect('/sepeda/table')

def sepeda_table(request):
    message = Sepeda.objects.all()
    response['sepeda_list'] = message
    html = 'sepeda/sepeda.html'
    return render(request, html , response)

def update(request):
    html = 'sepeda/update.html'
    response['updateform'] = UpdateForm
    return render(request, html, response)

def update_sepeda(request, station_id):
    sepeda = Sepeda.objects.raw('Select * from sepeda where nomor = %s', [nomor])
    initial_sepeda = {
        'nomor': sepeda[0].nomor,
        'merk': sepeda[0].merk,
        'jenis': sepeda[0].jenis,
        'status': sepeda[0].status,
        'id_stasiun': sepeda[0].id_stasiun,
        'no_kartu_penyumbang': sepeda[0].no_kartu_penyumbang
    }

    form = UpdateForm(request.POST)
    if(request.method == 'POST' and form.is_valid()):
        if form.has_changed():
            for c in form.changed_data:
                data = [form.cleaned_data.get(c), form.cleaned_data.get('station_id')]
                with connection.cursor() as cursor:
                    cursor.execute('UPDATE sepeda set %s = %s WHERE nomor = %s' %(c, '%s', '%s'), data)
        return HttpResponseRedirect('/sepeda/table')
    else:
        return HttpResponseRedirect('/sepeda/table')
