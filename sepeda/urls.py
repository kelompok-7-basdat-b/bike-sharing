from django.urls import path
from .views import sepeda, create_sepeda, sepeda_table, create, update, update_sepeda

app_name = 'sepeda'

urlpatterns = [
    path('', sepeda, name='sepeda'),
    path('create', create, name='create'),
    path('create_sepeda', create_sepeda, name='create_sepeda'),
    path('table', sepeda_table, name='table'),
    path('update', update, name='update'),
    path('update_sepeda', update_sepeda, name='update_sepeda'),
]
