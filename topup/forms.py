from django import forms
from .models import TopUpData

class TopUpForm(forms.ModelForm):
    nominal = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Amount to top up',
        }
    ))

    class Meta:
        model = TopUpData
        fields = ('nominal',)
