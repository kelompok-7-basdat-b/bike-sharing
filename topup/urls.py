from django.urls import path
from .views import topup

app_name = 'topup'

urlpatterns = [
    path('', topup, name = 'topup')
]
