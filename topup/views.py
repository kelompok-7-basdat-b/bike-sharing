from django.shortcuts import render

from .models import TopUpData
from .forms import TopUpForm

def topup(request):
    form = TopUpForm()
    return render(request, 'topup/topup.html', {'form':form})
