from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.db import connection
from .forms import CreateForm, UpdateForm
from .models import StationStation

response = {}

def station(request):
    message = StationStation.objects.all()
    response['station_list'] = message
    html = 'station/station.html'
    return render(request, html, response)

def create(request):
    html = 'station/create.html'
    response['createform'] = CreateForm
    return render(request, html, response)

def create_station(request):
    form = CreateForm(request.POST)
    if(request.method == 'POST' and form.is_valid()):
        prev_station_id = StationStation.objects.raw("Select station_id from station order by station_id DESC limit 1")
        station_id = int(prev_station_id[0].station_id) + 1
        address = form.cleaned_data['address']
        lat = form.cleaned_data['lat']
        lon = form.cleaned_data['lon']
        name = form.cleaned_data['name']
        
        with connection.cursor() as cursor:
            cursor.execute("insert into station values(%s, %s, %s, %s)", [station_id, name, address, lat, lon])
        return HttpResponseRedirect('/station/table')
    else:
        return HttpResponseRedirect('/station/table')

def station_table(request):
    message = StationStation.objects.all()
    response['station_list'] = message
    html = 'station/station.html'
    print(response)
    return render(request, html , response)

def update(request):
    html = 'station/update.html'
    response['updateform'] = UpdateForm
    return render(request, html, response)

def update_station(request, station_id):
    station = StationStation.objects.raw('Select * from station where station_id = %s', [station_id])
    initial_station = {
        'station_id': station[0].station_id,
        'name': station[0].name,
        'address': station[0].address,
        'lat': station[0].lat,
        'lon': station[0].lon
    }

    form = UpdateForm(request.POST)
    if(request.method == 'POST' and form.is_valid()):
        if form.has_changed():
            for c in form.changed_data:
                data = [form.cleaned_data.get(c), form.cleaned_data.get('station_id')]
                with connection.cursor() as cursor:
                    cursor.execute('UPDATE station set %s = %s WHERE station_id = %s' %(c, '%s', '%s'), data)
        return HttpResponseRedirect('/station/table')
    else:
        return HttpResponseRedirect('/station/table')