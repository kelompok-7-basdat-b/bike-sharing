from django.db import models

# Create your models here.

class StationStation(models.Model):
    address = models.TextField()
    lat = models.IntegerField()
    lon = models.IntegerField()
    name = models.CharField(max_length=50)
    station_id = models.IntegerField(primary_key=True)

    class Meta:
        db_table = 'station_station'