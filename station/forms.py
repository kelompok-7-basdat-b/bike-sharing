from django import forms

class CreateForm(forms.Form):
    error_messages = {
        'required': 'Please fill in the input',
    }

    name = forms.CharField(label='Name', max_length=50, required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    address = forms.CharField(label='Address', required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    lat = forms.IntegerField(label='Latitude', required=False, widget=forms.NumberInput(attrs={'class': 'form-control'}))
    lon = forms.IntegerField(label='Longitude', required=False, widget=forms.NumberInput(attrs={'class': 'form-control'}))

class UpdateForm(forms.Form):
    error_messages = {
        'required': 'Please fill in the input',
    }

    lat = forms.IntegerField(label='Latitude', required=False, widget=forms.NumberInput(attrs={'class': 'form-control'}))
    lon = forms.IntegerField(label='Longitude', required=False, widget=forms.NumberInput(attrs={'class': 'form-control'}))

