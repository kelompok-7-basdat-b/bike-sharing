from django.urls import path
from .views import station, create_station, station_table, create, update, update_station

app_name = 'station'

urlpatterns = [
    path('', station, name='station'),
    path('create', create, name='create'),
    path('create_station', create_station, name='create_station'),
    path('table', station_table, name='table'),
    path('update', update, name='update'),
    path('update_station', update_station, name='update_station')
]
