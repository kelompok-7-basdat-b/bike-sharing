from django.urls import path
from .views import create, update, assignment_list, assignment

app_name = 'assignment'

urlpatterns = [
    path('', assignment, name='assignment'),
    path('create/', create, name='create'),
    path('update/', update, name='update'),
    path('assignment_list/', assignment_list, name='assignment_list')
]
