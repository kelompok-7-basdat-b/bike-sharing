from django import forms

class CreateForm(forms.Form):
    error_messages = {
        'required': 'Please fill in the input',
    }

    officer = forms.ChoiceField(label='Officer', required=True)
    start_date = forms.DateField(label='Start Date', required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    end_date = forms.DateField(label='End Date', required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    station = forms.ChoiceField(label='Station', required=True)

class UpdateForm(forms.Form):
    error_messages = {
        'required': 'Please fill in the input',
    }

    officer = forms.ChoiceField(label='Officer', required=True)
    start_date = forms.DateField(label='Start Date', required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    end_date = forms.DateField(label='End Date', required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    station = forms.ChoiceField(label='Station', required=True)
