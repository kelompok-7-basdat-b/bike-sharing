from django.shortcuts import render, HttpResponseRedirect
from .forms import CreateForm, UpdateForm

# Create your views here.
response = {}

def create(request):
    form = CreateForm()
    response['form'] = form
    return render(request, 'create.html', response)

def update(request):
    form = UpdateForm()
    response['form'] = form
    return render(request, 'update.html', response)

def assignment_list(request):
    return render(request, 'assignment_list.html')

def assignment(request):
    return HttpResponseRedirect('assignment_list')
