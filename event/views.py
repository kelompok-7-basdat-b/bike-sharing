from django.shortcuts import render, HttpResponseRedirect
from .forms import CreateForm, UpdateForm

# Create your views here.
response = {}

def create(request):
    form = CreateForm()
    response['form'] = form
    return render(request, 'create.html', response)

def update(request):
    form = UpdateForm()
    response['form'] = form
    return render(request, 'update.html', response)

def event_list(request):
    return render(request, 'event_list.html')

def event(request):
    return HttpResponseRedirect('event_list')
