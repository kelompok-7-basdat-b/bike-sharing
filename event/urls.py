from django.urls import path
from .views import create, update, event_list, event

app_name = 'event'

urlpatterns = [
    path('', event, name='event'),
    path('create/', create, name='create'),
    path('update/', update, name='update'),
    path('event_list/', event_list, name='event_list')
]
