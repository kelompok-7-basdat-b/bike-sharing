from django import forms

class CreateForm(forms.Form):
    error_messages = {
        'required': 'Please fill in the input',
    }

    title = forms.CharField(label='Title', max_length=50, required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    description = forms.CharField(label='Description', required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    free = forms.ChoiceField(label='Gratis', required=True)
    start_date = forms.DateField(label='Start Date', required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    end_date = forms.DateField(label='End Date', required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    station = forms.ChoiceField(label='Station', required=True)

class UpdateForm(forms.Form):
    error_messages = {
        'required': 'Please fill in the input',
    }

    title = forms.CharField(label='Title', max_length=50, required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    description = forms.CharField(label='Description', required=False, widget=forms.TextInput(attrs={'class': 'form-control'}))
    free = forms.ChoiceField(label='Gratis', required=True)
    start_date = forms.DateField(label='Start Date', required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    end_date = forms.DateField(label='End Date', required=True, widget=forms.TextInput(attrs={'class': 'form-control'}))
    station = forms.ChoiceField(label='Station', required=True)
