"""main_app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('homepage.urls', namespace='homepage')),
    path('station/', include('station.urls', namespace='station')),
    path('sepeda/', include('sepeda.urls', namespace='sepeda')),
    path('event/', include('event.urls', namespace='event')),
    path('assignment/', include('assignment.urls', namespace='assignment')),
    path('login/', include('login.urls', namespace='login')),
    path('topup/', include('topup.urls', namespace='topup')),
    path('transaction/', include('transaction.urls', namespace='transaction')),
    path('laporan/', include('laporan.urls', namespace='laporan'))
]
