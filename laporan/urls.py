from django.urls import re_path
from .views import laporan

app_name = 'laporan'

urlpatterns = [
    re_path(r'^laporan$', laporan, name = 'laporan')
]
