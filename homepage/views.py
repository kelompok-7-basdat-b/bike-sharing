from django.shortcuts import render
from django.http import HttpResponse

def homepage(request):
    page_title = "Cyclink"
    response = {"title" : page_title}
    return render(request, 'homepage/index.html', response)
