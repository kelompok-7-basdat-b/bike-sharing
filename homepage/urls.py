from django.urls import re_path
from .views import homepage

app_name = 'homepage'

urlpatterns = [
    re_path(r'^$', homepage, name='homepage')
]
