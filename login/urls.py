from django.urls import path
from .views import login, registrasi, selrole

app_name = 'login'

urlpatterns = [
    path('selectrole/', selrole, name = 'selrole'),
    path('registrasi/', registrasi, name = 'registrasi'),
    path('login/', login, name ='login')
]
