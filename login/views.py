# import os

from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.db import connection
from .models import User,Profile,Person,Anggota,Petugas,Penugasan,Role
from .forms import RegistrasiForm, LoginForm, RoleForm


def selrole(request):
    form = RoleForm()
    if request.method == 'POST':
        if form.is_valid():
            result = form.cleaned_data.get('role')
            if result == 'Anggota':
                request.session['role'] = 'anggota'
            if result == 'Admin':
                request.session['role'] = 'admin'
            else:
                request.session['role'] = 'petugas'
        return HttpResponseRedirect(reverse('registrasi'))
    return render(request, 'login/select_role.html', {'form':form})

def registrasi(request):
    form = RegistrasiForm()
    if request.method == 'POST':
        if form.is_valid():
            result = [form.cleaned_data.get('nomor_ktp'), form.cleaned_data.get('nama_lengkap'),
                  form.cleaned_data.get('tanggal_lahir'), form.cleaned_data.get('email'),
                  form.cleaned_data.get('nomor_tlp'), form.cleaned_data.get('alamat')]
            request.session['no_ktp'] == form.cleaned_data.get('nomor_ktp')
            if request.session['role'] == 'anggota':
                with connection.cursor() as cursor:
                    cursor.execute("INSERT INTO PERSON values(%s, %s, %s, %s, %s, %s)", result)
                    cursor.execute("INSERT INTO ANGGOTA values(%s, 0, 0, %s)", [form.cleaned_data.get('nomor_ktp'), ])
                return HttpResponseRedirect(reverse('login'))
            if request.session['role'] == 'petugas':
                with connection.cursor() as cursor:
                    cursor.execute("INSERT INTO PERSON values(%s, %s, %s, %s, %s, %s)", result)
                    cursor.execute("INSERT INTO PETUGAS values(%s, 30000)", [form.cleaned_data.get('nomor_ktp'), ])
                return HttpResponseRedirect(reverse('login'))
            if request.session['role'] == 'admin':
                with connection.cursor() as cursor:
                    cursor.execute("INSERT INTO PERSON values(%s, %s, %s, %s, %s, %s)", result)
                    cursor.execute("INSERT INTO PETUGAS values(%s, 0)", [form.cleaned_data.get('nomor_ktp'), ])
                return HttpResponseRedirect(reverse('login'))
    return render(request, 'login/register.html', {'form':form})

def login(request):
    form = LoginForm()
    if request.method == 'POST':
        if form.is_valid():
            input = [form.cleaned_data.get('nomor_ktp'), form.cleaned_data.get('email')]
            request.session['no_ktp'] = result[0]
            user = Person.objects.raw('SELECT * FROM PERSON WHERE no_ktp = %s AND email = %s', input)
            if list(user):
                request.session['no_ktp'] = input[0]
                if list(Anggota.objects.raw('SELECT * FROM ANGGOTA WHERE no_ktp = %s', input[0])):
                    request.session['role'] = 'anggota'
                if list(Petugas.objects.raw('SELECT * FROM PETUGAS WHERE no_ktp = %s AND gaji = 0', input[0])):
                    request.session['role'] = 'admin'
                else:
                    request.session['role'] = 'petugas'
            return HttpResponseRedirect(reverse('homepage'))
    return render(request, 'login/login.html', {'form':form})

def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('homepage'))

# Create your views here.
