from django.db import models

class Profile(models.Model):
    nomor_ktp = models.CharField(max_length=300)
    nama_lengkap = models.CharField(max_length=300)
    tanggal_lahir = models.DateField()
    email = models.EmailField()
    nomor_tlp = models.CharField(max_length=300)
    alamat = models.CharField(max_length=300)

class User(models.Model):
    nomor_ktp = models.CharField(max_length=300)
    email = models.EmailField()

class Person(models.Model):
    ktp = models.CharField(max_length=300)
    nama = models.CharField(max_length=300)
    tgl_lahir = models.DateField()
    email = models.EmailField()
    no_telp = models.CharField(max_length=300)
    alamat = models.CharField(max_length=300)

    class Meta:
        managed = False
        db_table = 'person'

class Anggota(models.Model):
    no_kartu = models.CharField(max_length=300)
    saldo = models.IntegerField()
    points = models.IntegerField()
    ktp = models.CharField(max_length=300)

    class Meta:
        managed = False
        db_table = 'anggota'

class Petugas(models.Model):
    ktp = models.CharField(max_length=300)
    gaji = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'petugas'

class Penugasan(models.Model):
    ktp = models.CharField(max_length=300)
    start_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField()
    id_stasiun = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'penugasan'

class Laporan(models.Model):
    id_laporan = models.IntegerField()
    no_kartu_anggota = models.IntegerField()
    datetime_pinjam = models.DateTimeField()
    nomor_sepeda = models.IntegerField()
    id_stasiun = models.IntegerField()
    status = models.CharField(max_length=300)

    class Meta:
        managed = False
        db_table = 'laporan'

class Transaksi(models.Model):
    no_kartu_anggota = models.IntegerField()
    date_time = models.DateTimeField()
    jenis = models.CharField(max_length=300)
    nominal = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'transaksi'

class Role(models.Model):
    role = models.CharField(max_length=7)
