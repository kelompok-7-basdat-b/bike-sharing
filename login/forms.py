from django import forms
# from datetime import datetime
from .models import Profile,User,Role

# year_range = 100
# current_year = datetime.now().year

class RegistrasiForm(forms.ModelForm):
    nomor_ktp = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Identity Card Number',
        }
    ))
    nama_lengkap = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Full Name',
        }
    ))
    tanggal_lahir = forms.DateField(widget=forms.DateInput(
        attrs={
            'type': 'text',
            'onfocus': '(this.type="date")',
            'onblur': '(this.type="text")',
            'class': 'form-control',
            'placeholder': 'Birthday',
        }
    ))
    email = forms.EmailField(widget=forms.EmailInput(
        attrs={
            'type': 'email',
            'class': 'form-control',
            'placeholder': 'E-mail',
        }
    ))
    nomor_tlp = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Phone Number',
        }
    ))
    alamat = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Address',
        }
    ))

    class Meta:
        model = Profile
        fields = ('nomor_ktp', 'nama_lengkap', 'tanggal_lahir', 'email', 'nomor_tlp', 'alamat')

class LoginForm(forms.ModelForm):
    nomor_ktp = forms.CharField(widget=forms.TextInput(
        attrs={
            'class': 'form-control',
            'placeholder': 'Identity Card Number',
        }
    ))
    email = forms.EmailField(widget=forms.EmailInput(
        attrs={
            'type': 'email',
            'class': 'form-control',
            'placeholder': 'E-mail',
        }
    ))

    class Meta:
        model = User
        fields = ('nomor_ktp', 'email',)

class RoleForm(forms.ModelForm):
    ROLE_CHOICES = (
        ('Anggota', 'Anggota'),
        ('Petugas', 'Petugas'),
        ('Admin', 'Admin')
    )
    role = forms.ChoiceField(choices=ROLE_CHOICES)

    class Meta:
        model = Role
        fields = ('role',)
