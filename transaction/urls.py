from django.urls import re_path
from .views import transaction

app_name = 'transaction'

urlpatterns = [
    re_path(r'^transaction$', transaction, name = 'transaction')
]
