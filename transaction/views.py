from django.shortcuts import render

from .models import TransactionData

def transaction(request):
    data = TransactionData.objects.all()
    return render(request, 'transaction/transaction.html', {'Transaction':data})
