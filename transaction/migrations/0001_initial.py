# Generated by Django 2.1.1 on 2019-05-06 14:06

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TransactionData',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('no', models.CharField(max_length=300)),
                ('date', models.DateField()),
                ('type', models.CharField(max_length=300)),
                ('nominal', models.CharField(max_length=300)),
            ],
        ),
    ]
