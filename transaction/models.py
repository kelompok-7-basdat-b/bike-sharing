from django.db import models

class TransactionData(models.Model):
    no = models.CharField(max_length=300)
    date = models.DateField()
    type = models.CharField(max_length=300)
    nominal = models.CharField(max_length=300)
